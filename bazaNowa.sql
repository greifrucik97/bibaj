-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 16 Paź 2019, 18:58
-- Wersja serwera: 5.7.26
-- Wersja PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `biblioteka`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `authors`
--

DROP TABLE IF EXISTS `authors`;
CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `biography` longtext COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `authors`
--

INSERT INTO `authors` (`id`, `firstname`, `lastname`, `biography`) VALUES
(1, 'Bolesław', 'Prus', 'Polski pisarz, prozaik, nowelista i publicysta okresu pozytywizmu, współtwórca polskiego realizmu, kronikarz Warszawy, myśliciel i popularyzator wiedzy, działacz społeczny, propagator turystyki pieszej i rowerowej. Jeden z najwybitniejszych i najważniejszych pisarzy w historii literatury polskiej'),
(2, 'Jan', 'Paweł II', 'Polski duchowny rzymskokatolicki, biskup pomocniczy krakowski (1958–1964), a następnie arcybiskup metropolita krakowski (1964–1978), kardynał (1967–1978), Zastępca Przewodniczącego Konferencji Episkopatu Polski (1969–1978), 264. papież i 6. Suweren Państwa Watykańskiego w latach 1978–2005. Święty Kościoła katolickiego.\r\n\r\nPoeta i poliglota, a także aktor, dramaturg i pedagog. Filozof historii, fenomenolog, mistyk i przedstawiciel personalizmu chrześcijańskiego.'),
(3, 'Stefan', 'Żeromski', 'Polski prozaik, publicysta, dramaturg; pierwszy prezes polskiego PEN Clubu. Czterokrotnie nominowany do Nagrody Nobla w dziedzinie literatury (1921, 1922, 1923, 1924). Jeden z najwybitniejszych pisarzy polskich w historii['),
(4, 'Czesław', 'Miłosz', 'Polski poeta, prozaik, eseista, historyk literatury, tłumacz, dyplomata; w latach 1951–1993 przebywał na emigracji, do 1960 we Francji, następnie w Stanach Zjednoczonych; w Polsce do 1980 obłożony cenzurą; laureat Nagrody Nobla w dziedzinie literatury (1980); profesor Uniwersytetu Kalifornijskiego w Berkeley i Uniwersytetu Harvarda; pochowany w Krypcie Zasłużonych na Skałce; brat Andrzeja Miłosza. Uznawany za najwybitniejszego polskiego poetę XX wieku'),
(5, 'Adam', 'Mickiewicz', 'Polski poeta, działacz polityczny, publicysta, tłumacz, filozof, działacz religijny, mistyk, organizator i dowódca wojskowy, nauczyciel akademicki.'),
(6, 'Juliusz', 'Słowacki', 'Polski poeta, przedstawiciel romantyzmu, dramaturg i epistolograf. Obok Mickiewicza i Krasińskiego określany jako jeden z Wieszczów Narodowych. Twórca filozofii genezyjskiej (pneumatycznej), epizodycznie związany także z mesjanizmem polskim, był też mistykiem. Obok Mickiewicza uznawany powszechnie za największego przedstawiciela polskiego romantyzmu.'),
(7, 'Henryk', 'Sienkiewicz', 'Polski nowelista, powieściopisarz i publicysta; laureat Nagrody Nobla w dziedzinie literatury (1905) za całokształt twórczości[1], jeden z najpopularniejszych polskich pisarzy przełomu XIX i XX w');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `is_approved` int(11) NOT NULL,
  `content` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `description_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `is_approved`, `content`, `description_id`) VALUES
(1, 1, 0, 'Siemka', 1),
(2, 1, 1, 'co tam?', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `copies`
--

DROP TABLE IF EXISTS `copies`;
CREATE TABLE IF NOT EXISTS `copies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description_id` int(11) NOT NULL,
  `is_broken` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `copies`
--

INSERT INTO `copies` (`id`, `description_id`, `is_broken`) VALUES
(6, 1, 0),
(7, 1, 0),
(8, 1, 0),
(9, 1, 1),
(10, 1, 0),
(11, 1, 0),
(12, 1, 0),
(13, 2, 0),
(14, 2, 0),
(15, 2, 0),
(16, 2, 0),
(17, 3, 0),
(18, 3, 0),
(19, 3, 0),
(20, 3, 1),
(21, 3, 0),
(22, 4, 0),
(23, 4, 0),
(24, 5, 0),
(25, 6, 1),
(26, 6, 0),
(27, 6, 0),
(28, 6, 0),
(29, 6, 0),
(30, 6, 0),
(31, 7, 1),
(32, 7, 0),
(33, 8, 0),
(34, 8, 1),
(35, 8, 0),
(36, 9, 0),
(37, 9, 0),
(38, 10, 0),
(39, 10, 0),
(40, 10, 0),
(41, 10, 0),
(42, 16, 0),
(43, 16, 0),
(44, 16, 0),
(45, 17, 0),
(46, 17, 0),
(47, 18, 0),
(48, 18, 0),
(49, 18, 1),
(50, 19, 0),
(51, 19, 0),
(52, 19, 0),
(53, 19, 0),
(54, 19, 0),
(55, 20, 0),
(56, 20, 0),
(57, 26, 0),
(58, 26, 0),
(59, 26, 0),
(60, 27, 0),
(61, 27, 0),
(62, 28, 0),
(63, 29, 0),
(64, 29, 1),
(65, 29, 0),
(66, 29, 0),
(67, 30, 0),
(68, 30, 0),
(69, 31, 0),
(70, 31, 0),
(71, 31, 0),
(72, 32, 0),
(73, 32, 0),
(74, 33, 0),
(75, 33, 0),
(76, 33, 1),
(77, 33, 0),
(78, 33, 0),
(79, 34, 0),
(80, 34, 0),
(81, 34, 0),
(82, 35, 0),
(83, 41, 0),
(84, 41, 0),
(85, 41, 0),
(86, 41, 0),
(91, 42, 1),
(92, 42, 0),
(93, 42, 0),
(94, 42, 0),
(95, 43, 0),
(96, 43, 0),
(97, 44, 0),
(98, 45, 0),
(99, 45, 0),
(100, 45, 0),
(101, 45, 0),
(102, 46, 0),
(103, 46, 1),
(104, 46, 0),
(105, 46, 0),
(106, 47, 0),
(107, 47, 0),
(108, 47, 0),
(109, 47, 0),
(110, 47, 0),
(111, 48, 1),
(112, 48, 0),
(113, 49, 0),
(114, 50, 0),
(115, 50, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `descriptions`
--

DROP TABLE IF EXISTS `descriptions`;
CREATE TABLE IF NOT EXISTS `descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `descriptions`
--

INSERT INTO `descriptions` (`id`, `title`, `author_id`, `genre_id`, `publisher_id`, `state`) VALUES
(1, 'List Ojca Świętego Jana Pawła II do artystów', 2, 1, 1, 0),
(2, 'Encykliki Ojca Świętego Jana Pawła II', 2, 1, 2, 0),
(3, 'List Ojca Świętego Jana Pawła II do kapłanów na Wielki Czwartek 1991 roku', 2, 1, 1, 0),
(4, 'Orędzie Ojca Świętego Jana Pawła II na Światowy Dzień Pokoju 1 stycznia 2001 roku', 2, 1, 1, 0),
(5, 'List Ojca Świętego Jana Pawła II do kobiet', 2, 1, 1, 0),
(6, 'Katarynka', 1, 2, 3, 0),
(7, 'Opowiadania wieczorne', 1, 2, 4, 0),
(8, 'Placówka', 1, 3, 4, 0),
(9, 'Lalka', 1, 3, 5, 0),
(10, 'Emancypantki', 1, 3, 3, 0),
(16, 'Wierna rzeka', 3, 3, 6, 0),
(17, 'Promień', 3, 3, 4, 0),
(18, 'Duma o hetmanie', 3, 4, 7, 0),
(19, 'Sen o szpadzie i Sen o chlebie', 3, 5, 8, 0),
(20, 'Wisła', 3, 4, 9, 0),
(26, 'To', 4, 6, 10, 0),
(27, 'Węgry', 4, 6, 11, 0),
(28, 'Zdobycie władzy', 4, 3, 12, 0),
(29, 'Jak powinno być w niebie', 4, 6, 10, 0),
(30, 'Wilno Jerozolimą było', 4, 6, 13, 0),
(31, 'Pan Tadeusz czyli Ostatni zajazd na Litwie', 5, 6, 14, 0),
(32, 'Księgi narodu polskiego i pielgrzymstwa polskiego', 5, 7, 15, 0),
(33, 'Grażyna : powieść litewska', 5, 6, 16, 0),
(34, 'Dziady', 5, 8, 16, 0),
(35, 'Trybuna Ludów', 5, 7, 16, 0),
(41, 'Lilla Weneda : tragedia w pięciu aktach', 6, 8, 17, 0),
(42, 'Wiersze i poematy', 6, 6, 3, 0),
(43, 'Podróż na Wschód', 6, 6, 18, 0),
(44, 'Fantazy', 6, 8, 19, 0),
(45, 'Listy do matki', 6, 6, 19, 0),
(46, 'Potop', 7, 3, 3, 0),
(47, 'Pisma zapomniane i niewydane', 7, 3, 20, 0),
(48, 'Latarnik', 7, 2, 21, 0),
(49, 'Pan Wołodyjowski', 7, 3, 23, 0),
(50, 'Quo vadis', 7, 3, 22, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `favorites`
--

DROP TABLE IF EXISTS `favorites`;
CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `description_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `description_id`) VALUES
(1, 1, 1),
(2, 1, 50),
(3, 1, 17),
(4, 1, 20),
(5, 1, 28);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `genres`
--

DROP TABLE IF EXISTS `genres`;
CREATE TABLE IF NOT EXISTS `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `genres`
--

INSERT INTO `genres` (`id`, `name`) VALUES
(1, 'Życie religijne'),
(2, 'Nowela'),
(3, 'Powieść'),
(4, 'Poemat'),
(5, 'Proza'),
(6, 'Poezja'),
(7, 'Publicystyka'),
(8, 'Dramat');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hires`
--

DROP TABLE IF EXISTS `hires`;
CREATE TABLE IF NOT EXISTS `hires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `copy_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` date NOT NULL,
  `return_date` date NOT NULL,
  `hire_state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `hires`
--

INSERT INTO `hires` (`id`, `copy_id`, `user_id`, `created`, `return_date`, `hire_state_id`) VALUES
(1, 5, 1, '2019-10-01', '2019-11-30', 1),
(2, 16, 1, '2019-10-16', '2019-12-25', 1),
(3, 34, 1, '2019-10-04', '2019-10-30', 2),
(4, 19, 1, '2019-10-25', '2019-10-31', 2),
(5, 20, 1, '2019-10-21', '2019-10-26', 2),
(6, 22, 1, '2019-10-22', '2019-10-31', 3),
(7, 44, 1, '2019-10-01', '2019-10-29', 3),
(8, 25, 1, '2019-10-02', '2019-10-25', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hire_state`
--

DROP TABLE IF EXISTS `hire_state`;
CREATE TABLE IF NOT EXISTS `hire_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='wypożyczona lub zarezerwowana lub oddana';

--
-- Zrzut danych tabeli `hire_state`
--

INSERT INTO `hire_state` (`id`, `name`) VALUES
(1, 'Zarezerwowana'),
(2, 'Wypożyczona'),
(3, 'Oddana');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_negative` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `likes`
--

INSERT INTO `likes` (`id`, `description_id`, `user_id`, `is_negative`) VALUES
(1, 1, 1, 0),
(2, 1, 1, 0),
(3, 1, 1, 0),
(4, 1, 1, 0),
(5, 1, 1, 0),
(6, 1, 1, 0),
(7, 1, 1, 0),
(8, 1, 1, 1),
(9, 1, 1, 1),
(10, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `publishers`
--

DROP TABLE IF EXISTS `publishers`;
CREATE TABLE IF NOT EXISTS `publishers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `publishers`
--

INSERT INTO `publishers` (`id`, `name`) VALUES
(1, 'Watykan : [s. n.], (Watykan : Drukarnia Watykańska)'),
(2, 'Kraków : Dom Wydawniczy \"Rafael\" : Wydawnictwo AA'),
(3, 'Warszawa : Państ. Instytut Wydawniczy'),
(4, 'Warszawa : Gebethner i Wolff'),
(5, 'Kraków : Wydawnictwo Zielona Sowa'),
(6, 'Warszawa : Książka i Wiedza'),
(7, 'Warszawskich Pomocników Księgarskich'),
(8, 'Zakopane : nakładem Księgarni Podhalańskiej'),
(9, 'Warszawa ; Kraków : Towarzystwo Wydawnicze'),
(10, 'Kraków : \"Znak\"'),
(11, 'Paryż : Instytut Literacki'),
(12, 'Warszawa : Świat Książki'),
(13, 'Sejny : \"Pogranicze\"'),
(14, 'Kraków : nakł. Krakowskiej Spółki Wydawniczej,'),
(15, 'Paryż : Wydawnictwo Muzeum Adama Mickiewicza'),
(16, 'Warszawa : Czytelnik'),
(17, 'Kraków : Wydawnictwo M. Kot'),
(18, 'Jerozolima : Ministerstwo Wyznań Religijnych i Oświecenia Publicznego'),
(19, 'Wrocław : Wydawnictwo Zakładu Narodowego im. Ossolińskich'),
(20, 'Lwów : Drukarnia Zakładu Narodowego im. Ossolińskich'),
(21, 'Warszawa : Iskry'),
(22, 'Berlin : Schreitersche Verlagsbuchhandlung'),
(23, 'Warszawa : Redakcja \"Tygodnika Ilustrowanego\"');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `security_question` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `security_answer` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`, `is_admin`, `security_question`, `security_answer`) VALUES
(1, 'admin', 'admin', 'admin@admin.pl', 1, 'Jak mam na imie?', 'admin'),
(2, 'gracjan', 'gracjan', 'gracjan@gracja.pl', 0, 'Jak mam na imie?', 'gracjan'),
(3, 'piotr', 'piotr', 'piotr@piotr.pl', 0, 'Jak mam na imie?', 'piotr'),
(4, 'dominik', 'dominik', 'dominik@dominik.pl', 0, 'Jak mam na imie?', 'dominik'),
(5, 'sebastian', 'sebastian', 'sebastian@sebastian.pl', 0, 'Jak mam na imie?', 'sebastian');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
