package db;

import org.mariadb.jdbc.Driver;

import java.sql.*;

public class Dao {

    public ResultSet read() throws SQLException {
        new Driver();
        Connection connection = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/test",
                "root",
                "");

        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM tabela");
        statement.close();
        connection.close();
        return result;
    }
}
